package Casa;
import java.util.*; 
import Client.*;
import MainPack.GUIClass;
import MainPack.MainClass;

public class HeapClass extends MainClass{
	private Vector<ClientClass> clienti=new Vector<ClientClass>(); 
	GUIClass d;
	int minST;
	int maxST;
	public int STsum=0;
	int nrCase;
	int nrClienti;
	
	public HeapClass( String name, GUIClass d,int minST,int maxST,int nrCase,int nrClienti){
		setName( name );
		this.d=d;
		this.minST=minST;
		this.maxST=maxST;
		this.nrCase=nrCase;
		this.nrClienti=nrClienti;
	}
	
	public void run(){
		try{
			while( true ){
				int ST=(int) (Math.random()*(maxST-minST)+minST);
				STsum+=ST;
				sleep( ST );
				sterge_client();
			}
		 }
		 catch( InterruptedException e ){
			 System.out.println("Intrerupere");
			 System.out.println( e.toString());
		 	}
		 }
	
	public synchronized void adauga_client( ClientClass c ) throws InterruptedException{
		clienti.addElement(c);
		notifyAll();
	}
	
	public synchronized void sterge_client() throws InterruptedException{
		while( clienti.size() == 0 )
		wait();
		ClientClass c = ( ClientClass )clienti.elementAt(0);
		clienti.removeElementAt(0);
		d.tfRezultat.append("Client :"+Long.toString( c.getCID())+" a plecat de la "+getName()+"\n");
				if(c.getCID()<10) {
		if(getName().charAt(5)=='0') {String s=d.tfCoada0.getText();
				String s1=s.substring(26);
				String s2=s1.substring(2);
				d.tfCoada0.setText("Coada 0 a servit clientii:"+s2);
				}
				if(getName().charAt(5)=='1') {String s=d.tfCoada1.getText();
				String s1=s.substring(26);
				String s2=s1.substring(2);
				d.tfCoada1.setText("Coada 1 a servit clientii:"+s2);
				}
				if(getName().charAt(5)=='2') {String s=d.tfCoada2.getText();
				String s1=s.substring(26);
				String s2=s1.substring(2);
				d.tfCoada2.setText("Coada 2 a servit clientii:"+s2);
				}
				if(getName().charAt(5)=='3') {String s=d.tfCoada3.getText();
				String s1=s.substring(26);
				String s2=s1.substring(2);
				d.tfCoada3.setText("Coada 3 a servit clientii:"+s2);
				} }
				else {
					if(getName().charAt(5)=='0') {String s=d.tfCoada0.getText();
					String s1=s.substring(26);
					String s2=s1.substring(3);
					d.tfCoada0.setText("Coada 0 a servit clientii:"+s2);
					}
					if(getName().charAt(5)=='1') {String s=d.tfCoada1.getText();
					String s1=s.substring(26);
					String s2=s1.substring(3);
					d.tfCoada1.setText("Coada 1 a servit clientii:"+s2);
					}
					if(getName().charAt(5)=='2') {String s=d.tfCoada2.getText();
					String s1=s.substring(26);
					String s2=s1.substring(3);
					d.tfCoada2.setText("Coada 2 a servit clientii:"+s2);
					}
					if(getName().charAt(5)=='3') {String s=d.tfCoada3.getText();
					String s1=s.substring(26);
					String s2=s1.substring(3);
					d.tfCoada3.setText("Coada 3 a servit clientii:"+s2);
					} }
		notifyAll();
	}
	
	public synchronized long lungime_coada() throws InterruptedException{
		notifyAll();
		long size = clienti.size();
		return size;
	}
	public int getSTsum() {
		return STsum;
	}
}
