package Client;

public class ClientClass{

	private long cID;
	private int tsosire;
	private int tplecare;
	
	public ClientClass( long cID, int tsosire, int tplecare ){
		this.cID = cID;
		this.tsosire = tsosire;
		this.tplecare = tplecare;
	}
	
	public long getCID(){
		return cID;
	}
	
	public int getTsosire(){
		return tsosire;
	}
	
	public int getTplecare(){
		return tplecare;
	}
	
	public String toString(){
		return (Long.toString(cID)+" "+tsosire+" "+tplecare);
	}
} 