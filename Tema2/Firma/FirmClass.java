package Firma;
import Casa.*;
import Client.*;
import MainPack.*;

public class FirmClass extends MainClass{
	private HeapClass casa[];
	private int nrCase;
	static long ID =0;
	public int nrClienti;
	GUIClass d;
	int minAT;
	int maxAT;
	int ATsum=0;
	int STtot=0;
	float WT;

	public FirmClass(GUIClass d,HeapClass casa[], String name,int nrCase, int nrClienti,int minAT,int maxAT){
		setName( name );
		this.d=d ;
		this.nrCase = nrCase;
		this.casa = new HeapClass[nrCase];
		this.nrClienti = nrClienti;
		this.minAT=minAT;
		this.maxAT=maxAT;
		for( int i=0; i<nrCase; i++){
			this.casa[i] =casa[i] ; 	
		}
	}

	private int min_index (){
		int index = 0;
		try{
			long min = casa[0].lungime_coada();
			for( int i=1; i<nrCase; i++){
				long lung = casa[i].lungime_coada();
				if ( lung < min ){
					min = lung;
					index = i;
				}
			}
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
		return index;
	}

	public void run(){
		try{
			int i=0;
			while( i<nrClienti ){
				i++;
				int AT=(int)(Math.random()*(maxAT-minAT)+minAT) ;
				ATsum+=AT;
				ClientClass c = new ClientClass( ++ID,ATsum, ATsum+(int)WT );
				int m = min_index();
				d.tfRezultat.append("Client :" +Long.toString( ID )+" adaugat la CASA "+ Integer.toString(m)+"\n");
				matrix[m][(int)ID]=1;
				casa[m].adauga_client( c );
						if(m==0)d.tfCoada0.append (ID+" ");
						if(m==1)d.tfCoada1.append(ID+" ");
						if(m==2)d.tfCoada2.append(ID+" ");
						if(m==3)d.tfCoada3.append(ID+" ");
				sleep(AT);
			}
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
	}
	public void afis() {			 
		float ATmed= (float) (1.0*ATsum/(1.0*nrClienti));
		d.tfRezultat.append("\n"+"Avarage arriving time:"+ATmed);
		for (int i2=0;i2<nrCase;i2++)STtot+=casa[i2].STsum;
		float STmed= (float) (1.0*STtot/(1.0*nrClienti+1));
		d.tfRezultat.append("\n"+"Avarage serving time:"+STmed);
		if(STmed<ATmed)WT=STmed;
		else WT=STmed+(STmed-ATmed)*((nrClienti/nrCase)-1)/2;
		d.tfRezultat.append("\n"+"Avarage wating time:"+WT);}

	public void contCozi() {
		for (int i=0;i<nrCase;i++) {
			d.tfRezultat.append("\n"+"Casa "+i+ " a servit clientii: ");
			for(int j=1;j<=nrClienti;j++)if(matrix[i][j]==1) {
				d.tfRezultat.append(j+" ");
			}
		}
	}
	
}