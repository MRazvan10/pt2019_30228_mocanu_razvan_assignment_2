package MainPack;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.*;
import javax.swing.*;

import Firma.FirmClass;

public class GUIClass extends MainClass  implements ActionListener{

	public JFrame testareCozi;
	public JLabel minArrTime, maxArrTime, Rezultat,minServTime, maxServTime,numberQueues,nrClienti;
	public JTextField tfMinArrTime,tfMaxArrTime,tfMinServTime, tfMaxServTime,tfNumberQueues,tfNrClienti;
	public JTextArea tfRezultat;
	public JTextArea tfCoada0,tfCoada1,tfCoada2,tfCoada3;
	JButton jbnSet,jbnClear,jbnStop;
	Container cPane;
	int minAT=0;
	int maxAT=0;
	int nrQu=0;
	int minST=0;
	int maxST=0;
	int nrCl=0;
	FirmClass p;

	public GUIClass() {
		createGUI();
	}

	public void createGUI() {
		testareCozi = new JFrame("Testare Cozi");
		cPane = testareCozi.getContentPane();
		cPane.setLayout(new GridBagLayout());
		arrangeComponents();
		testareCozi.pack();
		testareCozi.setVisible(true);
	}

	public void arrangeComponents() {

		minArrTime = new JLabel("Timpul minim de sosire:");
		maxArrTime = new JLabel("Timpul maxim de sosire:");
		minServTime = new JLabel("Timpul minim de servire:");
		maxServTime = new JLabel("Timpul maxim de servire:");
		nrClienti = new JLabel("Numarul de clienti:");
		numberQueues = new JLabel("Numarul de cozi:");
		Rezultat = new JLabel("Rezultat:");

		tfMinArrTime = new JTextField(20);
		tfMaxArrTime = new JTextField(20);
		tfMinServTime = new JTextField(20);
		tfMaxServTime = new JTextField(20);
		tfNrClienti = new JTextField(20);
	    tfNumberQueues  = new JTextField(20);
		tfRezultat = new JTextArea(10,10);
		JScrollPane Scroll = new JScrollPane(tfRezultat);
		tfCoada0 = new JTextArea();
		tfCoada1 = new JTextArea();
		tfCoada2 = new JTextArea();
		tfCoada3 = new JTextArea();
		
		jbnSet = new JButton("Set");
		jbnClear = new JButton("Clear");
		jbnStop = new JButton("Stop");

		GridBagConstraints gridBagConstraintsx01 = new GridBagConstraints();
		gridBagConstraintsx01.gridx = 0;
		gridBagConstraintsx01.gridy = 0;
		cPane.add(minArrTime, gridBagConstraintsx01);

		GridBagConstraints gridBagConstraintsx02 = new GridBagConstraints();
		gridBagConstraintsx02.gridx = 1;
		gridBagConstraintsx02.gridy = 0;
		gridBagConstraintsx02.gridwidth = 2;
		cPane.add(tfMinArrTime, gridBagConstraintsx02);

		GridBagConstraints gridBagConstraintsx03 = new GridBagConstraints();
		gridBagConstraintsx03.gridx = 0;
		gridBagConstraintsx03.gridy = 1;
		cPane.add(maxArrTime, gridBagConstraintsx03);

		GridBagConstraints gridBagConstraintsx04 = new GridBagConstraints();
		gridBagConstraintsx04.gridx = 1;
		gridBagConstraintsx04.gridy = 1;
		gridBagConstraintsx04.gridwidth = 2;
		gridBagConstraintsx04.fill = GridBagConstraints.BOTH;
		cPane.add(tfMaxArrTime, gridBagConstraintsx04);
		
		GridBagConstraints gridBagConstraintsx09 = new GridBagConstraints();
		gridBagConstraintsx09.gridx = 0;
		gridBagConstraintsx09.gridy = 2;
		cPane.add(minServTime, gridBagConstraintsx09);

		GridBagConstraints gridBagConstraintsx10 = new GridBagConstraints();
		gridBagConstraintsx10.gridx = 1;
		gridBagConstraintsx10.gridy = 2;
		gridBagConstraintsx10.gridwidth = 2;
		gridBagConstraintsx10.fill = GridBagConstraints.BOTH;
		cPane.add(tfMinServTime, gridBagConstraintsx10);
		
		GridBagConstraints gridBagConstraintsx11 = new GridBagConstraints();
		gridBagConstraintsx11.gridx = 0;
		gridBagConstraintsx11.gridy = 3;
		cPane.add(maxServTime, gridBagConstraintsx11);

		GridBagConstraints gridBagConstraintsx12 = new GridBagConstraints();
		gridBagConstraintsx12.gridx = 1;
		gridBagConstraintsx12.gridy = 3;
		gridBagConstraintsx12.gridwidth = 2;
		gridBagConstraintsx12.fill = GridBagConstraints.BOTH;
		cPane.add(tfMaxServTime, gridBagConstraintsx12);
		
		GridBagConstraints gridBagConstraintsx13 = new GridBagConstraints();
		gridBagConstraintsx13.gridx = 0;
		gridBagConstraintsx13.gridy = 4;
		cPane.add(numberQueues, gridBagConstraintsx13);

		GridBagConstraints gridBagConstraintsx14 = new GridBagConstraints();
		gridBagConstraintsx14.gridx = 1;
		gridBagConstraintsx14.gridy = 4;
		gridBagConstraintsx14.gridwidth = 2;
		gridBagConstraintsx14.fill = GridBagConstraints.BOTH;
		cPane.add(tfNumberQueues, gridBagConstraintsx14);
		
		GridBagConstraints gridBagConstraintsx15 = new GridBagConstraints();
		gridBagConstraintsx15.gridx = 0;
		gridBagConstraintsx15.gridy = 5;
		cPane.add(nrClienti, gridBagConstraintsx15);

		GridBagConstraints gridBagConstraintsx16 = new GridBagConstraints();
		gridBagConstraintsx16.gridx = 1;
		gridBagConstraintsx16.gridy = 5;
		gridBagConstraintsx16.gridwidth = 2;
		gridBagConstraintsx16.fill = GridBagConstraints.BOTH;
		cPane.add(tfNrClienti, gridBagConstraintsx16);

		GridBagConstraints gridBagConstraintsx07 = new GridBagConstraints();
		gridBagConstraintsx07.gridx = 0;
		gridBagConstraintsx07.gridy = 6;
		cPane.add(jbnSet, gridBagConstraintsx07);
		
		GridBagConstraints gridBagConstraintsx05 = new GridBagConstraints();
		gridBagConstraintsx05.gridx = 0;
		gridBagConstraintsx05.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx05.gridy = 7;
		cPane.add(Rezultat, gridBagConstraintsx05);

		GridBagConstraints gridBagConstraintsx06 = new GridBagConstraints();
		gridBagConstraintsx06.gridx = 1;
		gridBagConstraintsx06.gridy = 7;
		gridBagConstraintsx06.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx06.gridwidth = 2;
		gridBagConstraintsx06.fill = GridBagConstraints.BOTH;
		cPane.add(Scroll, gridBagConstraintsx06);

		
		GridBagConstraints gridBagConstraintsx08 = new GridBagConstraints();
		gridBagConstraintsx08.gridy =8;
		cPane.add(jbnClear, gridBagConstraintsx08);
		
		
		GridBagConstraints gridBagConstraintsx17 = new GridBagConstraints();
		gridBagConstraintsx17.gridy =8;
		cPane.add(jbnStop, gridBagConstraintsx17);
		
		GridBagConstraints gridBagConstraintsx18 = new GridBagConstraints();
		gridBagConstraintsx18.gridx = 1;
		gridBagConstraintsx18.gridy = 9;
		gridBagConstraintsx18.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx18.gridwidth = 2;
		gridBagConstraintsx18.fill = GridBagConstraints.BOTH;
		cPane.add(tfCoada0, gridBagConstraintsx18);
		
		GridBagConstraints gridBagConstraintsx19 = new GridBagConstraints();
		gridBagConstraintsx19.gridx = 1;
		gridBagConstraintsx19.gridy = 10;
		gridBagConstraintsx19.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx19.gridwidth = 2;
		gridBagConstraintsx19.fill = GridBagConstraints.BOTH;
		cPane.add(tfCoada1, gridBagConstraintsx19);
		
		GridBagConstraints gridBagConstraintsx20 = new GridBagConstraints();
		gridBagConstraintsx20.gridx = 1;
		gridBagConstraintsx20.gridy = 11;
		gridBagConstraintsx20.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx20.gridwidth = 2;
		gridBagConstraintsx20.fill = GridBagConstraints.BOTH;
		cPane.add(tfCoada2, gridBagConstraintsx20);
		
		GridBagConstraints gridBagConstraintsx21 = new GridBagConstraints();
		gridBagConstraintsx21.gridx = 1;
		gridBagConstraintsx21.gridy = 12;
		gridBagConstraintsx21.insets = new Insets(5, 5, 5, 5);
		gridBagConstraintsx21.gridwidth = 2;
		gridBagConstraintsx21.fill = GridBagConstraints.BOTH;
		cPane.add(tfCoada3, gridBagConstraintsx21);

		jbnSet.addActionListener(this);
		jbnClear.addActionListener(this);
		jbnStop.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbnSet) {
			this.tfRezultat.append("\n"+"START:"+"\n");
			String first = tfMinArrTime.getText();
			String second = tfMaxArrTime.getText();
			String third = tfMinServTime.getText();
			String fourth = tfMaxServTime.getText();
			String fifth = tfNrClienti.getText();
			String sixth = tfNumberQueues.getText();
			this.minAT=Integer.parseInt(first);
			this.maxAT=Integer.parseInt(second);
			this.minST=Integer.parseInt(third);
			this.maxST=Integer.parseInt(fourth);
			this.nrCl=Integer.parseInt(fifth);
			this.nrQu=Integer.parseInt(sixth);
			this.p=startProgram(nrQu,minAT,maxAT,minST,maxST,nrCl,this);
		} else if (e.getSource() == jbnClear) {
			clear();
		} else if (e.getSource() == jbnStop) {
			stopProgram();
		}
	}

	public void clear() {
		this.tfRezultat.setText("Introduceti datele si apasati Set"+"\n");
		this.tfMinArrTime.setText("1000");
		this.tfMaxArrTime.setText("2000");
		this.tfMinServTime.setText("3000");
		this.tfMaxServTime.setText("4000");
		this.tfNumberQueues.setText("4");
		this.tfNrClienti.setText("30");
		this.tfCoada0.setText("Coada 0 a servit clientii:");
		this.tfCoada1.setText("Coada 1 a servit clientii:");
		this.tfCoada2.setText("Coada 2 a servit clientii:");
		this.tfCoada3.setText("Coada 3 a servit clientii:");
} 	
	
	public void stopProgram() {
		stopProgram(p,this);
	}
	
	public void clearResult() {
		tfRezultat.setText("");
	}
}