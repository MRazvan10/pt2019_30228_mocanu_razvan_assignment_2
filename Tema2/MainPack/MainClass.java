package MainPack;

import Casa.*;
import Firma.*;

public class MainClass extends Thread{
	
	public FirmClass p;
	public HeapClass c[];
	
	public int[][] matrix=new int[100][10000];
	
	public static void main( String args[] ){
		GUIClass d = new GUIClass();
		d.clear();
		d.tfRezultat.setText("Introduceti datele si apasati Set"+"\n");
		d.tfMinArrTime.setText("1000");
		d.tfMaxArrTime.setText("2000");
		d.tfMinServTime.setText("3000");
		d.tfMaxServTime.setText("4000");
		d.tfNumberQueues.setText("4");
		d.tfNrClienti.setText("30");
		d.tfCoada0.setText("Coada 0 a servit clientii:");
		d.tfCoada1.setText("Coada 1 a servit clientii:");
		d.tfCoada2.setText("Coada 2 a servit clientii:");
		d.tfCoada3.setText("Coada 3 a servit clientii:");
 }
	
	FirmClass startProgram(int nrQu,int minAT,int maxAT,int minST,int maxST,int nrCl,GUIClass d) {
		this.c = new HeapClass[ nrQu ];
		for( int i=0; i<nrQu; i++){
			c[ i ] = new HeapClass("Casa "+Integer.toString( i ),d,minST,maxST,nrQu,nrCl);
			c[ i ].start();
		} 
		this.p = new FirmClass(d,c,"Producator",nrQu,nrCl,minAT,maxAT);
		p.start();
		return p;
	}
	
	public void stopProgram(FirmClass p,GUIClass d) {
		d.tfRezultat.append("\n"+"STOP:"+"\n");
		p.afis();
		p.contCozi();
		p.nrClienti=-1;
	}

}